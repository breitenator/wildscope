# README #

Wildscope is an open source project that is aimed for assembling and analysis of WildFly application server metrics ([http://wildfly.org](htto://wildfly.org)). Wildscope is deployable application that runs in WildFly server using Derby database. It gather statistics on WildFly deployments activity (such as deployment EJB method invocation count, deployment EJB method elapse time, etc.)

### How do I get set up? ###

#### Prerequisites

* JDK8
* Distribution of WildFly 8.1 application server (http://wildfly.org)
* Apache Maven 3.x
* JavaDB instance to test datasource (note that now JavaDB is part of JDK)
* Derby client driver installed into you Maven repository

```
#!xml
<groupId>org.apache.derby</groupId>
<artifactId>derbyclient</artifactId>
<version>10.11.1.1</version>
```

#### Configuration

###### Wildfly Server

1. Unpack Wildfly distribution into some folder (further $WILDFLY_HOME)
2. Add admin user ($WILDFLY_HOME/bin/add-user)
3. Start the applicatin server ($WILDFLY_HOME/bin/standalone.sh)

###### Java Database

1. Prepared database instance (read [JavaDB reference](http://docs.oracle.com/javadb/10.10.1.2/getstart/twwdactivity4.html))
2. Create repo database schema 

###### Application Setup 

1. Make clone of project source 
2. Check database connection setting in runtime-cfg/pom.xml
3. Build project and deploy it to the application server

```
#!bash
mvn clean install -P wildfly
```
4. Check application availablity

```
#!curl
curl http://localhost:8080/wildscope/api/entity
```

#### Enabling statistics on Wildfly 

###### Enabling common EJB statistics
Open jconsole and execute:
```
/subsystem=ejb3/:write-attribute(name="enable-statistics", value="true")
```

###### Enabling system JPA statistics
Open jconsole and execute:
```
/deployment=wildscope.war/subsystem=jpa/hibernate-persistence-unit=wildscope.war#production/:write-attribute(name="statistics-enabled", value="true")
```