package org.wildscope.metrics;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.script.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TestUnwrapList {
    ScriptEngine scriptEngine = null;
    Bindings bindings = null;

    @Before
    public void initScriptEngine() {
        ScriptEngineManager engineManager = new ScriptEngineManager();
        scriptEngine = engineManager.getEngineByName("javascript");
        ScriptContext newContext = new SimpleScriptContext();
        bindings = newContext.getBindings(ScriptContext.ENGINE_SCOPE);
    }


    @Test
    public void unwrap() throws ScriptException, IOException {

        List<String> result = new ArrayList<>();

        BufferedReader in = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/data/MemoryInfo.json")));
        StringBuilder input = new StringBuilder();

        for (String s = in.readLine(); s != null; s = in.readLine()) {
            input.append(s);
        }

        bindings.put("input", input.toString());
        bindings.put("path", "/result/heap-memory-usage");
        bindings.put("result", result);
        scriptEngine.eval(new InputStreamReader(this.getClass().getResourceAsStream("/UnwrapList.js")), bindings);
        Assert.assertTrue(result.size() > 0);
    }
}
