package org.wildscope.metrics;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class TestJsonParse {

    @Test
    public void parseJson() throws IOException {
        Map<String, Object> result = new HashMap<>();
        String method = "init";

        BufferedReader in = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/data/Ejb3Info.json")));
        StringBuilder input = new StringBuilder();

        for (String s = in.readLine(); s != null; s = in.readLine()) {
            input.append(s);
        }

        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(input.toString()).getAsJsonObject();

        if (json.has("result")) {
            json = json.getAsJsonObject("result");
            if (json.has("methods") && json.get("methods").isJsonObject()) {
                JsonElement m = json.getAsJsonObject("methods").get(method);
                if (m != null) {
                    m.getAsJsonObject().entrySet().forEach(e -> result.put(e.getKey(), e.getValue().toString()));
                }
            }
        }

        Assert.assertTrue(result.size() > 0);
    }

}
