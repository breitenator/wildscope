package org.wildscope.request;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.wildscope.service.common.ResourceClient;
import org.wildscope.service.common.ResourceRequest;

import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;

public class ManagementServiceIT {

    ResourceClient client = null;

    @Before
    public void initClient() {
        client = new ResourceClient("localhost", 9990);
        client.setCredentials("admin", "admin");
    }

    @Test
    public void serverInfo() {
        ResourceRequest resource = new ResourceRequest();
        resource.setAddress(new String[]{"core-service", "platform-mbean", "type", "memory"});
        Response response = client.sendRequest(resource);
        Assert.assertEquals(HttpURLConnection.HTTP_OK, response.getStatus());
    }

    @Test
    public void deployments() {
        ResourceRequest resource = new ResourceRequest();
        resource.setAddress(new String[]{"deployment"});
        Response response = client.sendRequest(resource);
        Assert.assertEquals(HttpURLConnection.HTTP_OK, response.getStatus());
    }

}
