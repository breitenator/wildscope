create sequence objects_seq as bigint start with 1;
create sequence metrics_seq as bigint start with 1;
create sequence samples_seq as bigint start with 1;
create sequence metrics_values_seq as bigint start with 1;
create sequence settings_seq as bigint start with 1;


create table objects(
  object_id	bigint not null primary key,
  object_name	varchar(255) not null,
  object_parent_id bigint,
  object_type	varchar(255) not null
);

create table metrics(
  metric_id	bigint not null primary key,
  metric_name	varchar(255) not null,
  object_type	varchar(255) not null
);

create unique index metrics_uc on metrics (metric_name, object_type);

create table samples(
  sample_id	bigint not null primary key,
  sampled_at timestamp not null
);

create table metrics_values(
  metric_value_id	bigint not null primary key,
  object_id	bigint not null,
  metric_id	bigint not null,
  sample_id	bigint not null,
  metric_value varchar(255),
  metric_total_value varchar(255),
  constraint objects_fk foreign key (object_id) references objects(object_id),
  constraint metrics_fk foreign key (metric_id) references metrics(metric_id),
  constraint sample_fk foreign key (sample_id) references samples(sample_id)
);

--create unique index metrics_uc on metrics (metric_name);
create unique index metrics_values_uc on metrics_values (metric_id, object_id, sample_id);


create table settings(
  setting_id bigint not null primary key,
  setting_name varchar(255),
  setting_value  varchar(255)
);

create unique index settings_uc on settings(setting_name);