insert into settings(
  setting_id, setting_name, setting_value
) values (
  next value for settings_seq, 'hostname', 'localhost'
);

insert into settings(
  setting_id, setting_name, setting_value
) values (
  next value for settings_seq, 'port', '9990'
);

insert into settings(
  setting_id, setting_name, setting_value
) values (
  next value for settings_seq, 'username', 'admin'
);

insert into settings(
  setting_id, setting_name, setting_value
) values (
  next value for settings_seq, 'password', 'admin'
);

insert into settings(
  setting_id, setting_name, setting_value
) values (
  next value for settings_seq, 'assembling_frequency', '5'
);

insert into settings(
  setting_id, setting_name, setting_value
) values (
  next value for settings_seq, 'highcharts_refresh_frequency', '5'
);