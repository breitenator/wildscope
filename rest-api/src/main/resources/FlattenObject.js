(function () {
    function flatten(prefix, object) {
        for (p in object) {
            if (Object.prototype.toString.call(object[p]) === '[object Array]') {
                var numItems = object[p].length
                for (var i = 0; i < numItems; i++) {
                    if (typeof(object[p][i]) === 'object') {
                        flatten(prefix + (p + '/') + (i + '/'), object[p][i]);
                    } else {
                        result[prefix + (p + '/' + i)] = object[p][i];
                    }
                }
            } else if (typeof(object[p]) === 'object') {
                flatten(prefix + (p + '/'), object[p])
            } else {
                result[prefix + p] = object[p];
            }
        }
    }
    flatten('/', JSON.parse(input))
}())