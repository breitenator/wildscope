(function() {
    function unwrap(prefix, object) {
        for (var p in object) {
            if (Object.prototype.toString.call(object[p]) === '[object Array]') {
                if (path === prefix + p) for (var i = 0; i < object[p].length; i++) {
                    result.add(object[p][i])
                } else {
                    unwrap(prefix + (p + '/'), object[p][i]);
                }
            } else if (typeof(object[p]) === 'object') {
                if (path === prefix + p) for (var item in object[p]) {
                    result.add(item)
                } else {
                    unwrap(prefix + (p + '/'), object[p])
                }
            }
        }
    }
    unwrap('/', JSON.parse(input))
}())
