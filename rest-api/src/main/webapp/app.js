(function() {
    var app = angular.module('wildscope', [
        'ngRoute',

        'chartDirectives',
        'loadingModal',

        'welcomeCtrl',
        'setupCtrl',
        'schedulerCtrl',
        'invocationsReportCtrl',
        'elapsedTimeReportCtrl',
        'transactionCountReportCtrl',
        'menuCtrl'
    ]);

    app.config(function ($routeProvider) {
        $routeProvider.when("/", {
            templateUrl: "views/welcome.html"
        });

        $routeProvider.when("/setup", {
            templateUrl: "views/setup.html"
        });

        $routeProvider.when("/scheduler", {
            templateUrl: "views/scheduler.html"
        });

        $routeProvider.when("/invocationsReport", {
            templateUrl: "views/invocationsReport.html"
        });

        $routeProvider.when("/elapsedTimeReport", {
            templateUrl: "views/elapsedTimeReport.html"
        });

        $routeProvider.when("/transactionCountReport", {
            templateUrl: "views/transactionCountReport.html"
        });

        $routeProvider.otherwise({
            templateUrl: "views/welcome.html"
        });
    });
}());