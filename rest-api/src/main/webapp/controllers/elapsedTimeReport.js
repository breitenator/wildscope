(function() {
    var app = angular.module('elapsedTimeReportCtrl', []);

    app.controller('elapsedTimeReportCtrl', function($scope, $http) {
        $scope.objectId = 0;
        $scope.loadedSnapshotId = 0;

        $http.get('api/info/ejbWithCategories', {
            params: {
                objectTypes: ['ejb3:singleton-bean','ejb3:stateless-session-bean']
            }
        }).success(function (res) {
            var s2data = [];

            for (var i in res) {
                s2data.push({
                    text: i,
                    children: res[i]
                })
            }
            $("#objects").select2({
                data : s2data
            });
            $("#objects").on("change", function(e) {
                $scope.changeObjects(e.added.id, e.added.text);
            });
        }).error(function (data, status, headers, config) {
            bootbox.alert(data, function () {});
        });


        $scope.changeObjects = function(id, text) {
            $scope.$apply(function () {
                $scope.objectId = id;
                $scope.loadedSnapshotId = 0;
            });
        };
    });
} ());