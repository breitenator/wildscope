(function() {
    var app = angular.module('schedulerCtrl', []);

    app.controller("schedulerCtrl", function ($http, $scope, $rootScope, $timeout) {
        $scope.deployments = [];
        $scope.status = "";
        $scope.disableLoop = false;
        $scope.status = "";

        $http.get('api/settings').success(function (data) {
            for(var i = 0; i < data.length; i++) {
                $scope[data[i].name] = data[i].value;
            }
        }).error(function (data, status, headers, config) {
            console.log(data);
        });

        $scope.findDeployments = function(){
            $http.post('api/objects/initDeployments')
                .success(function (data) {
                    $scope.deployments = data;

                    for(var i = 0; i < $scope.deployments.length; i++) {
                        $scope.deployments[i].status = "inactive";
                    }
                })
                .error(function (error) {
                    console.log(error);
                });
        };

        $http.get('api/platform/status').success(function (data) {
            $scope.status = data;
        }).error(function (data, status, headers, config) {
            console.log(data);
        }).then( function() {
            if($scope.status == 'running') {
                $scope.findDeployments();
                $scope.checkStatus();
            }
        });

        $scope.start = function(deploymentName) {

            var activeDeployments = [deploymentName];

            var data = {
                    frequency: $scope.frequency,
                    deployments: activeDeployments
                };

            $http.post('api/sampling/start',data)
                .success(function (data) {
                })
                .error(function (error) {
                    console.log(error);
                }).finally(function () {
                });

            for(var i = 0; i < $scope.deployments.length; i++) {
                if($scope.deployments[i].objectName == deploymentName) {
                    $scope.deployments[i].status = "active";
                }
            }
        };

        $scope.stop = function(pause, deploymentName) {
            $scope.disableLoop = true;

            $http.post('api/sampling/stop', [deploymentName, pause])
                .success(function (data) {})
                .error(function (error) {
                    console.log(error);
                }).finally(function () {
                });

            for(var i = 0; i < $scope.deployments.length; i++) {
                if($scope.deployments[i].objectName == deploymentName) {
                    if(pause == 'true') {
                        $scope.deployments[i].status = "pause";
                    } else {
                        $scope.deployments[i].status = "inactive";
                    }
                }
            }
        };

        $scope.checkStatus = function() {
            if(!$scope.disableLoop) {
                $rootScope.isLoadingWindowBlocked = true;
                $http.get('api/sampling/status').success(function (data) {
                    for(var i = 0; i < $scope.deployments.length; i++) {
                        for (var property in data) {
                            if (data.hasOwnProperty(property)) {
                                if($scope.deployments[i].objectName == property) {
                                    $scope.deployments[i].status = data[property].status;
                                }
                            }
                        }
                    }

                }).error(function (res, status, headers, config) {
                    console.log(res);
                });
                $timeout($scope.checkStatus, 1000);
            }
        };

        $scope.$on('$routeChangeStart', function(next, current) {
            $rootScope.isLoadingWindowBlocked = false;
            $scope.disableLoop = true;
        });
    });
} ());