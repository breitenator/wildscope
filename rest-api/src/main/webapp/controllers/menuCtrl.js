(function() {
    var app = angular.module('menuCtrl', []);

    app.controller("menuCtrl", function ($scope, $location) {
        $scope.isActive = function (viewLocation) {
            var active = (viewLocation === $location.path());
            return active;
        };
    });
} ());