(function() {
    var app = angular.module('invocationsReportCtrl', []);

    app.controller('invocationsReportCtrl', function($scope, $http, $location, $interval) {
        //$scope.disableLoop = false;
        $scope.objectId = 0;
        //$scope.status = 'active';
        $scope.loadedSnapshotId = 0;
        //$scope.chart = null;

        $http.get('api/info/ejbWithCategories', {
            params: {
                objectTypes: ['ejb3:singleton-bean','ejb3:stateless-session-bean']
            }
        }).success(function (res) {
            var s2data = [];

            for (var i in res) {
                s2data.push({
                    text: i,
                    children: res[i]
                })
            }
            $("#objects").select2({
                data : s2data
            });
            $("#objects").on("change", function(e) {
                $scope.changeObjects(e.added.id, e.added.text);
            });
        }).error(function (data, status, headers, config) {
            bootbox.alert(data, function () {});
        });


        $scope.changeObjects = function(id, text) {
            /*
            if ($scope.worker != null) {
                $interval.cancel($scope.worker);
            }
            */

            $scope.$apply(function () {
                $scope.objectId = id;
                $scope.loadedSnapshotId = 0;
            });
        };
/*
        $scope.checkStatus = function() {
            $http.get('api/sampling/status').success(function (data) {
                $scope.status = 'inactive';

                for (var property in data) {
                    if (data.hasOwnProperty(property)) {
                        if(data[property].status == 'active') {
                            $scope.status = 'active'
                        }
                    }
                }
            }).error(function (res, status, headers, config) {
                bootbox.alert(res, function () {});
            });
        };

        $scope.loopDataRefresh = function(id, text, status) {
            if($scope.objectId == id && status == 'active') {
                $scope.loadData(id, text);
                $interval(function() {$scope.loopDataRefresh(id, text, $scope.status)}, $scope.refreshRate);
                $scope.checkStatus();
            }
        };

        $scope.$on('$routeChangeStart', function(next, current) {
            $scope.disableLoop = true;
        });
*/

/*
        $scope.updateChartData = function(callback) {
            if($("#objects").val() > 0) {

                $http.get('api/charts/nextSeries/EJBMethodsStatistics', {
                    params: {
                        parentObjectId: $("#objects").val(),
                        sampleFromId: $scope.loadedSnapshotId,
                        metricName: 'invocations'
                    }
                }).success(function (data) {
                    //console.log(data);

                    $scope.loadedSnapshotId = data.sampleIdTo;

                    var increaseSnapshot = false;
                    for (var property in data.series) {
                        if(data.series[property].length > 0) {
                            increaseSnapshot = true;

                            break;
                        }
                    }

                    if(increaseSnapshot) {
                        callback(data);

                        $scope.loadedSnapshotId = $scope.loadedSnapshotId + 1;
                    }

                }).error(function (data, status, headers, config) {
                    bootbox.alert(data, function () {});
                });
            }
        }
*/
    });
} ());