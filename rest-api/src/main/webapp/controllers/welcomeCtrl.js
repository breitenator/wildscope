(function() {
    var app = angular.module('welcomeCtrl', []);

    app.controller("welcomeCtrl", ["$http", "$scope", "$timeout", "$rootScope",  function ($http, $scope, $timeout, $rootScope) {
        $scope.disableLoop = false;
        $scope.status = "";

        function loadServerInfo(url) {
            $http.get(url).success(function (data) {
                $scope.serverEnvironment = [];
                for (var p in data) {
                    $scope.serverEnvironment.push({
                        property: p,
                        value: data[p]
                    })
                }
            }).error(function (res, status, headers, config) {
                console.log(res);
            });
        }

        $scope.loadBasicInfo = function() {
            if($scope.status == 'running') {
                loadServerInfo('api/platform/server-environment?mode=basic');
                $scope.serverInfoMode = 'basic';
            }
        };

        $scope.loadDetailedInfo = function() {
            if($scope.status == 'running') {
                loadServerInfo('api/platform/server-environment?mode=detailed');
                $scope.serverInfoMode = 'detailed';
            }
        };

        var gaugeOptions = {

            chart: {
                type: 'solidgauge'
            },

            title: null,

            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },

            tooltip: {
                enabled: false
            },

            // the value axis
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickPixelInterval: 400,
                tickWidth: 0,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },

            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };

        // The speed gauge
        $('#gauge-cpu').highcharts(Highcharts.merge(gaugeOptions, {
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: 'CPU'
                }
            },

            credits: {
                enabled: false
            },

            series: [{
                name: 'Speed',
                data: [0],
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:silver">Percent (%)</span></div>'
                },
                tooltip: {
                    valueSuffix: ' %'
                }
            }]

        }));

        // The RPM gauge
        $('#gauge-ram').highcharts(Highcharts.merge(gaugeOptions, {
            yAxis: {
                min: 0,
                max: 1000,
                title: {
                    text: 'Memory'
                }
            },

            series: [{
                name: 'RAM',
                data: [0],
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
                    '<span style="font-size:12px;color:silver">MB</span></div>'
                },
                tooltip: {
                    valueSuffix: ' MB'
                }
            }]

        }));

        // The RPM gauge
        $('#gauge-peak-threads').highcharts(Highcharts.merge(gaugeOptions, {
            yAxis: {
                min: 0,
                max: 500,
                title: {
                    text: 'Thread Count'
                }
            },

            series: [{
                name: 'Thread Count',
                data: [0],
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
                    '<span style="font-size:12px;color:silver">Threads</span></div>'
                },
                tooltip: {
                    valueSuffix: ' Threads'
                }
            }]

        }));

        // Bring life to the dials
        function refreshDials() {
            $rootScope.isLoadingWindowBlocked = true;

            // Average CPU load
            $http.get('api/platform/average-load').success(function (data) {
                chart = $('#gauge-cpu').highcharts();
                if (chart) {
                    point = chart.series[0].points[0];
                    point.update(parseFloat(data["system-load-average"].toFixed(2)))
                }
                if(point.y == -1) {
                    $('#gauge-cpu').highcharts().destroy();
                    document.getElementById("gauge-cpu").innerHTML = "CPU statistics is not available";
                }
            }).error(function (res, status, headers, config) {
                chart = $('#gauge-cpu').highcharts();
                if (chart) {
                    point = chart.series[0].points[0];
                    point.update(0)
                }
                console.error(res);
            });

            // RAM
            $http.get('api/platform/memory').success(function (data) {
                chart = $('#gauge-ram').highcharts();
                if (chart) {
                    point = chart.series[0].points[0];
                    point.update(data["heap-memory-usage:used"] / (1024 * 1024))
                }
            }).error(function (res, status, headers, config) {
                chart = $('#gauge-ram').highcharts();
                if (chart) {
                    point = chart.series[0].points[0];
                    point.update(0)
                }
                console.error(res);
            });

            // Peak Threads
            $http.get('api/platform/threading').success(function (data) {
                chart = $('#gauge-peak-threads').highcharts();
                if (chart) {
                    point = chart.series[0].points[0];
                    point.update(data["thread-count"])
                }
            }).error(function (res, status, headers, config) {
                chart = $('#gauge-peak-threads').highcharts();
                if (chart) {
                    point = chart.series[0].points[0];
                    point.update(0)
                }
                console.error(res);
            });
        }

        $scope.$on('$routeChangeStart', function(next, current) {
            $rootScope.isLoadingWindowBlocked = false;
            $scope.disableLoop = true;
        });

        $http.get('api/platform/status').success(function (data) {
            $scope.status = data;
        }).error(function (data, status, headers, config) {
            console.log(data);
        }).then( function() {

            if($scope.status == 'running') {

                $scope.loadBasicInfo();
                setInterval(function() {
                    if(!$scope.disableLoop) {
                        refreshDials()
                    }
                }, 2000);
                refreshDials()
            }
        });
    }]);
} ());