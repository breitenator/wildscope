(function() {
    var app = angular.module('setupCtrl', []);

    app.controller("setupCtrl", function ($http, $scope) {
        $http.get('api/settings').success(function (data) {
            for(var i = 0; i < data.length; i++) {
                $scope[data[i].name] = data[i].value;
            }
        }).error(function (data, status, headers, config) {
            console.log(data);
        });

        $scope.submit = function() {
            var data = {
                hostname: $scope.hostname,
                port: $scope.port,
                username: $scope.username,
                password: $scope.password,
                assembling_frequency: $scope.assembling_frequency,
                highcharts_refresh_frequency: $scope.highcharts_refresh_frequency
            };
            $http.post('api/settings',data)
                .success(function (data) {
                    bootbox.alert("Changes are saved!", function () {});
                })
                .error(function (error) {
                    console.log(error);
                }).finally(function () {
                });
        }
    });
} ());