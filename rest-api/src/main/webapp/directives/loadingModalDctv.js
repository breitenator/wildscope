(function() {
    var app = angular.module('loadingModal', []);

    app.directive('loadingModal',  ['$http' ,function ($http) {
        return {
            restrict: 'E',
            templateUrl: 'views/loading-modal.html',

            controller: ['$scope','$rootScope', function($scope, $rootScope) {

                $scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };

                $scope.$watch($scope.isLoading, function (loading) {
                    if (loading) {
                        if (!$scope.isLoadingWindowBlocked) {
                            $('#staticLoadingModal').modal('show');
                        }
                    } else {
                        $('#staticLoadingModal').modal('hide');
                    }

                    //console.log("isStaticDataLoaded [" + $scope.isStaticDataLoaded + "] blockLoadingModals [" + $scope.isLoadingWindowBlocked + "]");
                });
            }]
        };

    }]);
}());