(function() {
    var app = angular.module('chartDirectives', []);

    app.directive("chartReport", ["$compile","$http", "$rootScope", function ($compile, $http, $rootScope) {
        return {
            restrict: 'A',
            scope: false,
            link: function (scope, element, attrs, ctrl, http) {
                var lastPoint = new Map;
                var objectId = 0;
                var repopulateSeries = true;
                var refreshRate = 5000;
                var chartType = '';

                $http.get('api/settings/highcharts_refresh_frequency'
                ).success(function (data) {
                        refreshRate = data * 1000;
                }).error(function (data, status, headers, config) {
                    bootbox.alert(data, function () {
                    });
                });


                var getConfiguration = function() {
                    $http.get('api/charts/configuration/' + attrs['chartName']
                    ).success(function (data) {
                            chartType = data.chartType;
                            repopulateSeries = data.repopulateSeries;

                            //options.title = {text: data.chartTitle};
                            options.title.text = data.chartTitle;
                            if(data.xAxisType.length > 0) {
                                options.xAxis.type = data.xAxisType;
                            }

                            if(data.xAxisTickPixelInterval > 0) {
                                options.xAxis.tickPixelInterval = data.xAxisTickPixelInterval;
                            }

                            if(data.xAxisCategories != null && data.xAxisCategories.length > 0) {
                                options.xAxis.categories = data.xAxisCategories;
                            }

                            if(data.yAxisTitle.length > 0) {
                                options.yAxis.title.text = data.yAxisTitle;
                            }


                        }).error(function (data, status, headers, config) {
                            bootbox.alert(data, function () {
                            });
                        });
                };

                Highcharts.setOptions({
                    global: {
                        useUTC: false
                    }
                });

                var loadedSnapshotId = 0;

                var updateData = function () {
                    $rootScope.isLoadingWindowBlocked = true;

                    if(objectId > 0) {
                        $http.get('api/charts/nextSeries/' + attrs['chartName'], {
                            params: {
                                objectId: objectId,
                                sampleFromId: loadedSnapshotId
                            }
                        }).success(function (dataIn) {
                            loadedSnapshotId = dataIn.sampleIdTo;

                            var increaseSnapshot = false;
                            for (var property in dataIn.series) {
                                if (dataIn.series[property].length > 0) {
                                    increaseSnapshot = true;

                                    break;
                                }
                            }

                            if (increaseSnapshot) {
                                loadedSnapshotId = loadedSnapshotId + 1;
                            }

                            if (chart.series.length == 0) {
                                for (var property in dataIn.series) {
                                    if (dataIn.series.hasOwnProperty(property)) {
                                        var series = {
                                            id: property,
                                            name: property,
                                            data: dataIn.series[property]
                                        };
                                        chart.addSeries(series);
                                        lastPoint.put(property, 0);
                                    }
                                }
                            } else {
                                for (var property in dataIn.series) {
                                    var addSeries = true;
                                    for (var seriesIndex = 0; seriesIndex < chart.series.length; seriesIndex++) {
                                        if (dataIn.series.hasOwnProperty(property)) {
                                            if (chart.series[seriesIndex].name == property) {
                                                addSeries = false;
                                            }
                                        }
                                    }

                                    if(addSeries) {
                                        var series = {
                                            id: property,
                                            name: property,
                                            data: []//dataIn.series[property]
                                        };
                                        chart.addSeries(series);
                                        lastPoint.put(property, 0);
                                    }
                                }

                                //chart series loop
                                for (var seriesIndex = 0; seriesIndex < chart.series.length; seriesIndex++) {
                                    //data property loop - seeks for same method name
                                    for (var property in dataIn.series) {
                                        if (dataIn.series.hasOwnProperty(property)) {
                                            if (chart.series[seriesIndex].name == property) {
                                                //data loop - seeks for appropriate point
                                                for (var j = 0; j < dataIn.series[property].length; j++) {

                                                    if(repopulateSeries == true) {
                                                        //console.log("[" + repopulateSeries + "][" + seriesIndex + "][" + dataIn.series[property][j][0] + "]");

                                                        chart.series[seriesIndex].setData([dataIn.series[property][j][0]], true);
                                                    } else {
                                                        if (dataIn.series[property][j][0] > lastPoint.get(property)) {
                                                            lastPoint.put(property, dataIn.series[property][j][0]);
                                                            chart.series[seriesIndex].addPoint([dataIn.series[property][j][0], dataIn.series[property][j][1]], true, false);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            $rootScope.isLoadingWindowBlocked = false;
                        }).error(function (data, status, headers, config) {
                            bootbox.alert(data, function () {
                            });
                        });
                    }
                };

                var chart = null;
                var intervalId = null;
                var lastPoint = [];
                var options = {
                    chart: {
                        type: chartType, //attrs['chartType'],
                        animation: Highcharts.svg, // don't animate in old IE
                        marginRight: 10,
                        renderTo: attrs['id'],
                        events: {
                            load: function () {
                                intervalId = setInterval(updateData, refreshRate);
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {},
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    legend: {
                        enabled: true,
                        layout: 'vertical',
                        align: 'center',
                        verticalAlign: 'bottom',
                        borderWidth: 0
                    },
                    exporting: {
                        enabled: false
                    },
                    series: []
                };

                scope.$on('$routeChangeStart', function(next, current) {
                    if (angular.isDefined(intervalId)) {
                        clearInterval(intervalId);
                    }
                });

                scope.$watch("objectId", function(newValue, oldValue){
                    if (chart != null) {
                        if (angular.isDefined(intervalId)) {
                            clearInterval(intervalId);
                        }
                        chart.destroy();
                        lastPoint = new Map;
                        loadedSnapshotId = 0;
                    }
                    objectId = newValue;

                    getConfiguration();

                    options.chart.type = chartType;
                    if(chartType == 'spline') {
                        chart = new Highcharts.StockChart(options);
                    } else {
                        chart = new Highcharts.Chart(options);
                    }
                    updateData();

                });
            }
        };
    }]);
}());
