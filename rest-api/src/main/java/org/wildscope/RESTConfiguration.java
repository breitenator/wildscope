package org.wildscope;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class RESTConfiguration extends Application {
}
