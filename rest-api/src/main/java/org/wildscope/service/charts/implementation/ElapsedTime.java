package org.wildscope.service.charts.implementation;

import org.wildscope.service.charts.Chart;
import org.wildscope.service.charts.ChartConfig;
import org.wildscope.service.charts.ChartSeries;
import org.wildscope.service.statistics.StatisticsService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class ElapsedTime implements Chart {
    private List<String> metricNames;

    @Inject
    private StatisticsService statisticsService;

    public ElapsedTime() {
        metricNames = new ArrayList<>();
        metricNames.add("execution-time");
    }

    @Override
    public String getUnitName() {
        return "ElapsedTime";
    }

    @Override
    public ChartConfig getHighchartsConfiguration() {
        ChartConfig config = new ChartConfig(
                "bar",
                "EJB Methods Elapsed Time",
                true
        );

        String[] xAxisCategories = {""};
        config.setxAxisCategories(xAxisCategories);

        config.setyAxisTitle("milliseconds");

        return config;
    }

    @Override
    public ChartSeries getNextHighchartsSeries(
            long objectId,
            long sampleFromId) {
        return statisticsService.findObjectStatisticsByMetricNamesLatest(objectId, sampleFromId, metricNames);
    }
}
