package org.wildscope.service.charts;

public class ChartConfig {
    private String chartType;
    private String chartTitle;
    private boolean repopulateSeries;

    //xAxis
    private String xAxisType = "";
    private int xAxisTickPixelInterval = 0;
    private String[] xAxisCategories = null;

    //yAxis
    private String yAxisTitle = "";

    public ChartConfig(
            String chartType,
            String chartTitle,
            boolean repopulateSeries
    ) {
        this.chartType = chartType;
        this.chartTitle = chartTitle;
        this.repopulateSeries = repopulateSeries;
    }

    public void setxAxisTickPixelInterval(int xAxisTickPixelInterval) {
        this.xAxisTickPixelInterval = xAxisTickPixelInterval;
    }

    public void setxAxisType(String xAxisType) {
        this.xAxisType = xAxisType;
    }

    public void setxAxisCategories(String[] xAxisCategories) {
        this.xAxisCategories = xAxisCategories;
    }

    public void setyAxisTitle(String yAxisTitle) {
        this.yAxisTitle = yAxisTitle;
    }
}
