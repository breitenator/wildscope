package org.wildscope.service.charts;

public interface Chart {
    public String getUnitName();

    public ChartConfig getHighchartsConfiguration();

    public ChartSeries getNextHighchartsSeries(
            long objectId,
            long sampleFromId
    );
}
