package org.wildscope.service.charts;

import org.wildscope.logging.Logged;
import org.wildscope.service.charts.Chart;
import org.wildscope.service.charts.ChartConfig;
import org.wildscope.service.charts.ChartSeries;

import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.logging.Logger;

@Path("charts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
@Logged
public class ChartsResource {
    @Inject
    private Instance<Chart> highchartsUnits;

    @Inject
    Logger log;

    @Path("/configuration/{chartName}")
    @GET
    public ChartConfig getChartConfiguration(
            @PathParam("chartName") String chartName
    ) {
        log.finer("getChartConfiguration [" + chartName + "]");

        for(Chart chart : highchartsUnits) {
            if (chart.getUnitName().equals(chartName)) {
                return chart.getHighchartsConfiguration();
            }
        }

        return null;
    }

    @Path("/nextSeries/{chartName}")
    @GET
    public ChartSeries getNextChartSeries(
            @PathParam("chartName") String chartName,
            @QueryParam("objectId") long objectId,
            @QueryParam("sampleFromId") long sampleFromId
    ) {
        log.finer("getNextChartSeries [" + chartName + "][" + objectId + "][" + sampleFromId + "]");

        for(Chart chart : highchartsUnits) {
            if (chart.getUnitName().equals(chartName)) {
                return chart.getNextHighchartsSeries(objectId, sampleFromId);
            }
        }

        return null;
    }
}
