package org.wildscope.service.charts.implementation;

import org.wildscope.service.charts.Chart;
import org.wildscope.service.charts.ChartConfig;
import org.wildscope.service.charts.ChartSeries;
import org.wildscope.service.statistics.StatisticsService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class TransactionCount implements Chart {
    private List<String> metricNames;

    @Inject
    private StatisticsService statisticsService;

    public TransactionCount() {
        metricNames = new ArrayList<>();
        metricNames.add("completed-transaction-count");
        metricNames.add("successful-transaction-count");
    }

    @Override
    public String getUnitName() {
        return "TransactionsCount";
    }

    @Override
    public ChartConfig getHighchartsConfiguration() {
        ChartConfig config = new ChartConfig(
                "line",
                "Transaction Count",
                false
        );

        config.setxAxisType("datetime");
        config.setxAxisTickPixelInterval(150);

        return config;
    }

    @Override
    public ChartSeries getNextHighchartsSeries(
            long objectId,
            long sampleFromId
    ) {
        return statisticsService.findNextSeries(objectId, 0, sampleFromId, metricNames, true);
    }
}