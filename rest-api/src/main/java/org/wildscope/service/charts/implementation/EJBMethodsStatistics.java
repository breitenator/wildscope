package org.wildscope.service.charts.implementation;

import org.wildscope.service.charts.ChartConfig;
import org.wildscope.service.charts.ChartSeries;
import org.wildscope.service.charts.Chart;
import org.wildscope.service.statistics.StatisticsService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class EJBMethodsStatistics implements Chart {
    private List<String> metricNames;

    @Inject
    private StatisticsService statisticsService;

    public EJBMethodsStatistics() {
        metricNames = new ArrayList<>();
        metricNames.add("invocations");
    }

    @Override
    public String getUnitName() {
        return "EJBMethodsStatistics";
    }

    @Override
    public ChartConfig getHighchartsConfiguration() {
        ChartConfig config = new ChartConfig(
                "line",
                "EJB Methods Invocation Statistics",
                false
        );

        config.setxAxisType("datetime");
        config.setxAxisTickPixelInterval(150);

        return config;
    }

    @Override
    public ChartSeries getNextHighchartsSeries(
            long objectId,
            long sampleFromId
    ) {
        return statisticsService.findNextSeries(0, objectId, sampleFromId, metricNames, false);
    }
}
