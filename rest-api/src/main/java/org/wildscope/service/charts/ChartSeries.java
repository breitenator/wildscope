package org.wildscope.service.charts;

import java.util.*;

public class ChartSeries {
    private long sampleIdFrom;
    private long sampleIdTo;

    private Map<String,List<List>> series;

    public ChartSeries() {
        series = new HashMap<>();
    }

    public long getSampleIdFrom() {
        return sampleIdFrom;
    }

    public void setSampleIdFrom(long sampleIdFrom) {
        this.sampleIdFrom = sampleIdFrom;
    }

    public long getSampleIdTo() {
        return sampleIdTo;
    }

    public void setSampleIdTo(long sampleIdTo) {
        this.sampleIdTo = sampleIdTo;
    }

    public Map<String,List<List>> getSeries() {
        return series;
    }
}
