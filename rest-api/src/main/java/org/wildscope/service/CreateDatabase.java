package org.wildscope.service;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Startup
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class CreateDatabase {
    @Inject
    private DataSource dataSource;

    @Inject
    private Logger log;

    @PostConstruct
    public void createDatabase() {
        try {
            log.info("--------------------------- Creating Database ---------------------------");
            log.info("[" + dataSource.getConnection().getMetaData().getURL() + "]");
        } catch (SQLException e) {
            log.severe(String.format("Failed to obtain database connection: %s", e.getMessage()));
            throw new RuntimeException(e);
        }

        Flyway flyway = new Flyway();
        //flyway.setInitOnMigrate(true);
        flyway.setDataSource(dataSource);
        for (MigrationInfo i : flyway.info().all()) {
            log.info(String.format("db migration task: version %s, source %s, %s", i.getVersion(), i.getScript(), i.getDescription()));
        }
        flyway.migrate();
    }
}
