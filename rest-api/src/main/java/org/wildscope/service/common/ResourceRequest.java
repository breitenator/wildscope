package org.wildscope.service.common;


import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ResourceRequest {

    @SerializedName("operation")
    private String operation = "read-resource";

    @SerializedName("include-runtime")
    private Boolean includeRuntime = true;

    @SerializedName("address")
    private String[] address;

    @SerializedName("json.pretty")
    private int prettyPrint = 1;

    public String[] getAddress() {
        return address;
    }

    public void setAddress(String[] address) {
        this.address = address;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
