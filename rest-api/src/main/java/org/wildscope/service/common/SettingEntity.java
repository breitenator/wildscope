package org.wildscope.service.common;

import javax.persistence.*;

@Entity
@Table(name="settings")
public class SettingEntity {
    @SequenceGenerator(name="settings_seq", sequenceName="settings_seq", allocationSize = 1)
    @Id
    @GeneratedValue(generator="settings_seq")
    @Column(name="setting_id")
    private Long id;

    @Column(name="setting_name")
    private String name;

    @Column(name="setting_value")
    private String value;

    public SettingEntity() {}

    public SettingEntity(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
