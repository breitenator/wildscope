package org.wildscope.service.common;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="samples")
public class Sample {

    @SequenceGenerator(name="samples_seq", sequenceName="samples_seq", allocationSize = 1)
    @Id
    @GeneratedValue(generator="samples_seq")
    @Column(name="sample_id")
    private Long sampleId;

    @Column(name="sampled_at")
    private Timestamp sampledAt;

    public Long getSampleId() {
        return sampleId;
    }

    public void setSampleId(Long sampleId) {
        this.sampleId = sampleId;
    }

    public Timestamp getSampledAt() {
        return sampledAt;
    }

    public void setSampledAt(Timestamp sampledAt) {
        this.sampledAt = sampledAt;
    }

    @Override
    public String toString() {
        return "Sample{" +
                "sampleId=" + sampleId +
                ", sampledAt=" + sampledAt +
                '}';
    }
}
