package org.wildscope.service.common;


import javax.persistence.*;

@Entity
@Table(name="metrics")
public class Metric {

    @SequenceGenerator(name="metrics_seq", sequenceName="metrics_seq", allocationSize = 1)
    @Id
    @GeneratedValue(generator="metrics_seq")
    @Column(name="metric_id")
    private Long metricId;

    @Column(name="metric_name")
    private String metricName;

    @Column(name="object_type")
    private String objectType;

    public Metric(){}

    public Metric(String name, String objectType) {
        this.setMetricName(name);
        this.setObjectType(objectType);
    }

    public Long getMetricId() {
        return metricId;
    }

    public void setMetricId(Long metricId) {
        this.metricId = metricId;
    }

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Metric) && Metric.class.cast(o).hashCode() == this.hashCode();
    }

    @Override
    public int hashCode() {
        return "".concat(this.getObjectType()).concat(this.getMetricName()).hashCode();
    }

    @Override
    public String toString() {
        return "Metric{" +
                "metricId=" + metricId +
                ", metricName='" + metricName + '\'' +
                ", objectType='" + objectType + '\'' +
                '}';
    }
}
