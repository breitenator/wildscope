package org.wildscope.service.common;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="objects")
public class ObjectEntity {

    @SequenceGenerator(name="objects_seq", sequenceName="objects_seq", allocationSize = 1)
    @Id
    @GeneratedValue(generator="objects_seq")
    @Column(name="object_id")
    private Long objectId;

    @Column(name="object_name")
    private String objectName;

    @Column(name="object_type")
    private String objectType;

    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "object_parent_id")
    private List<ObjectEntity> children = new LinkedList<>();

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "object_parent_id",insertable=false,updatable=false)
    private ObjectEntity parentObjectEntity;

    public ObjectEntity() {}

    public ObjectEntity(String objectName, String objectType) {
        this.objectName = objectName;
        this.objectType = objectType;
    }

    public ObjectEntity getRoot() {
        ObjectEntity objectEntity = this;
        while(true) {
            if(objectEntity.getParent() == null) break;
            objectEntity = objectEntity.getParent();
        }
        return objectEntity;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public ObjectEntity getParent() {
        return parentObjectEntity;
    }

    public void setParent(ObjectEntity parentObjectEntity) {
        this.parentObjectEntity = parentObjectEntity;
        if(parentObjectEntity != null)
            parentObjectEntity.getChildren().add(this);
    }

    public List<ObjectEntity> getChildren() {
        return children;
    }

    public void setChildren(List<ObjectEntity> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof ObjectEntity) && ObjectEntity.class.cast(o).hashCode() == this.hashCode();
    }

    public String path() {
        String path = "";
        if(this.getParent() != null) {
            path += getParent().path();
        }

        return path.concat("/").concat(this.getObjectName()).concat(":").concat(this.getObjectType());
    }

    @Override
    public int hashCode() {
        return path().hashCode();
    }

    @Override
    public String toString() {
        return "ObjectEntity{" +
                "objectId=" + objectId +
                ", objectName='" + objectName + '\'' +
                ", objectType='" + objectType + '\'' +
                '}';
    }
}