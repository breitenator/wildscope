package org.wildscope.service.common;

import javax.persistence.*;

@Entity
@Table(name="metrics_values")
public class MetricValue {
    @SequenceGenerator(name="metrics_values_seq", sequenceName="metrics_values_seq", allocationSize = 1)
    @Id
    @GeneratedValue(generator="metrics_values_seq")
    @Column(name="metric_value_id")
    private Long metricValueId;

    @Column(name="object_id")
    private Long objectId;

    @Column(name="metric_id")
    private Long metricId;

    @Column(name="sample_id")
    private Long sampleId;

    @Column(name="metric_value")
    private String metricValue;

    @Column(name="metric_total_value")
    private String metricTotalValue;

    public MetricValue () {}

    public MetricValue(Long objectId, Long metricId, Long sampleId, String metricValue, String metricTotalValue) {
        this.objectId = objectId;
        this.metricId = metricId;
        this.sampleId = sampleId;
        this.metricValue = metricValue;
        this.metricTotalValue = metricTotalValue;
    }



    public String getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(String metricValue) {
        this.metricValue = metricValue;
    }

    public Long getMetricValueId() {
        return metricValueId;
    }

    public void setMetricValueId(Long metricValueId) {
        this.metricValueId = metricValueId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getMetricId() {
        return metricId;
    }

    public void setMetricId(Long metricId) {
        this.metricId = metricId;
    }

    public Long getSampleId() {
        return sampleId;
    }

    public void setSampleId(Long sampleId) {
        this.sampleId = sampleId;
    }

    public String getMetricTotalValue() {
        return metricTotalValue;
    }

    public void setMetricTotalValue(String metricTotalValue) {
        this.metricTotalValue = metricTotalValue;
    }

    @Override
    public String toString() {
        return "MetricValue{" +
                "metricValueId=" + metricValueId +
                ", objectId=" + objectId +
                ", metricId=" + metricId +
                ", sampleId=" + sampleId +
                ", metricValue='" + metricValue + '\'' +
                ", metricTotalValue='" + metricTotalValue + '\'' +
                '}';
    }
}

