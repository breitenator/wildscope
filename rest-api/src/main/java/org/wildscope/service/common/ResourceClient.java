package org.wildscope.service.common;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;
import org.wildscope.provider.JsonObjectReader;
import org.wildscope.provider.JsonObjectWriter;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ResourceClient {

    private Client client;
    private String baseUrl;
    private AuthScope authScope;
    private DefaultHttpClient httpClient;

    public ResourceClient(String hostname, int port) {
        httpClient = new DefaultHttpClient();
        authScope = new AuthScope(hostname, port);
        client = new ResteasyClientBuilder().httpEngine(new ApacheHttpClient4Engine(httpClient)).build();
        client.register(JsonObjectReader.class);
        client.register(JsonObjectWriter.class);
        baseUrl = String.format("http://%1$s:%2$d/management", hostname, port);
    }

    public void setCredentials(String username, String password) {
        if (httpClient.getCredentialsProvider() == null) {
            httpClient.setCredentialsProvider(new BasicCredentialsProvider());
        }
        httpClient.getCredentialsProvider().setCredentials(authScope, new UsernamePasswordCredentials(username, password));
    }

    public Response sendRequest(ResourceRequest resource) {
        Invocation.Builder target = client.target(baseUrl).request(MediaType.APPLICATION_JSON);
        return target.post(Entity.json(resource));
    }
}
