package org.wildscope.service.objects;

import org.wildscope.provider.Pretty;
import org.wildscope.service.settings.SettingsService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Path("platform")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class PlatformResource {
    @Inject
    private SettingsService settingsService;

    @Inject
    ObjectInfoService objectInfoService;

    @GET
    @Path("status")
    @Produces(MediaType.TEXT_PLAIN)
    public String serverStatus() {
        try {
            URL url = new URL(String.format("http://%1$s:%2$d/management", settingsService.findHostName(), settingsService.findPort()));
            HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
            urlConn.connect();
        } catch (IOException e) {
            return "stopped";
        }

        try {
            return (String) formatResponse(objectInfoService.getObject(new String[]{})).get("server-state");
        } catch (Exception e) {
            return "stopped";
        }
    }

    @GET
    @Pretty
    @Path("server-environment")
    public Map<String, Object> getServerEnvironment(@QueryParam("mode") @DefaultValue("basic") String mode) {

        String[] address;

        if (mode.equalsIgnoreCase("basic")) {
            address = new String[0];
        } else if (mode.equalsIgnoreCase("detailed")) {
            address = new String[]{"core-service", "server-environment"};
        } else {
            throw new IllegalArgumentException(mode);
        }

        return formatResponse(objectInfoService.getObject(address));
    }

    @GET
    @Pretty
    @Path("average-load")
    public Map<String, Object> getAverageLoadInfo() {
        return formatResponse(objectInfoService.getObject(new String[]{"core-service", "platform-mbean", "type", "operating-system"}));
    }

    @GET
    @Pretty
    @Path("memory")
    public Map<String, Object> getMemoryInfo() {
        return formatResponse(objectInfoService.getObject(new String[]{"core-service", "platform-mbean", "type", "memory"}));
    }

    @GET
    @Pretty
    @Path("threading")
    public Map<String, Object> getThreadingInfo() {
        return formatResponse(objectInfoService.getObject(new String[]{"core-service", "platform-mbean", "type", "threading"}));
    }

    private Map<String, Object> formatResponse(Map<String, Object> data) {
        Map<String, Object> result = data.entrySet().stream()
                .map(e -> {
                    Map.Entry<String, Object> entry = null;
                    if (e.getKey().contains("/result/")) {
                        entry = Collections.singletonMap(e.getKey().replace("/result/", "").replace("/", ":"),
                                e.getValue()).entrySet().iterator().next();
                    } else {
                        entry = Collections.singletonMap("", new Object()).entrySet().iterator().next();
                    }
                    return entry;
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        result.remove("");

        return new TreeMap<>(result);
    }
}
