package org.wildscope.service.objects;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.wildscope.service.common.ObjectEntity;
import org.wildscope.service.common.ResourceClient;
import org.wildscope.service.common.ResourceRequest;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.util.*;

@Stateless
public class ObjectInfoService {

    @Inject
    private ScriptEngine scriptEngine;

    @Inject
    private ResourceClient client;

    public List<String> listDeployments() {
        return listItems(new String[]{"deployment"}, "/result/deployment");
    }

    public List<String> listSubsystems(String module) {
        return listItems(new String[]{"deployment", module, "subsystem"}, "/result/subsystem");
    }

    public List<String> listSubsystem(String module, String subsystem) {
        return listItems(new String[]{"deployment", module, "subsystem", subsystem}, "/result");
    }

    public List<String> listCategory(String module, String subsystem, String category) {
        return listItems(new String[]{"deployment", module, "subsystem", subsystem}, "/result/".concat(category));
    }

    public Map<String, Object> getObject(String[] path) {
        Map<String, Object> result = new HashMap<>();
        Response response = sendRequest(path);
        Bindings bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE);

        bindings.put("input", response.readEntity(String.class));
        bindings.put("result", result);
        try {
            scriptEngine.eval(new InputStreamReader(this.getClass().getResourceAsStream("/FlattenObject.js")), bindings);
        } catch (ScriptException e) {
            e.printStackTrace();
        }

        return result;
    }

    public Map<String, Object> getObject(String module, String subsystem, String category, String name) {
        return getObject(new String[]{"deployment", module, "subsystem", subsystem, category, name});
    }

    public Map<String, Object> getMethod(String module, String subsystem, String category, String object, String method) {
        Map<String, Object> result = new HashMap<>();
        Response response = sendRequest(new String[]{"deployment", module, "subsystem", subsystem, category, object});

        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(response.readEntity(String.class)).getAsJsonObject();

        if (json.has("result")) {
            json = json.getAsJsonObject("result");
            if (json.has("methods") && json.get("methods").isJsonObject()) {
                JsonElement m = json.getAsJsonObject("methods").get(method);
                if (m != null) {
                    m.getAsJsonObject().entrySet().forEach(e -> result.put(e.getKey(), e.getValue().toString()));
                }
            }
        }
        return result;
    }

    private Response sendRequest(String[] address) {
        ResourceRequest request = new ResourceRequest();
        request.setAddress(address);
        Response response = client.sendRequest(request);
        if (HttpURLConnection.HTTP_OK != response.getStatus()) {
            throw new IllegalStateException("server responded " + response.getStatus() + " to request " + request);
        }

        return response;
    }

    private List<String> listItems(String[] address, String path) {
        List<String> result = new ArrayList<>();
        Response response = sendRequest(address);
        Bindings bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE);
        bindings.put("input", response.readEntity(String.class));
        bindings.put("path", path);
        bindings.put("result", result);

        try {
            scriptEngine.eval(new InputStreamReader(this.getClass().getResourceAsStream("/UnwrapList.js")), bindings);
        } catch (ScriptException e) {
            e.printStackTrace();
        }


        return result;
    }

    public List<ObjectEntity> itemListToObjectEntityTree() {

        List<ObjectEntity> deployments = new LinkedList<>();

        listDeployments().stream().forEach((d) -> {
            ObjectEntity deployment = new ObjectEntity(d,"deployment");
            deployments.add(deployment);

            List<String> subsystem = listSubsystems(d);
            if (!subsystem.isEmpty()) {
                subsystem.stream().forEach(s -> {
                    listSubsystem(d, s).forEach(c -> {
                        listCategory(d, s, c).stream().forEach(it -> {
                            ObjectEntity level2 = new ObjectEntity(it,s.concat(":").concat(c));
                            level2.setParent(deployment);

                            if (s.equalsIgnoreCase("ejb3")) {
                                Set<String> methods = new HashSet<String>();
                                getObject(d, s, c, it)
                                        .keySet().stream()
                                        .filter(k -> k.startsWith("/result/methods"))
                                        .forEach(m -> {
                                            methods.add(m.split("/")[3]);
                                        });

                                methods.stream().forEach(m -> {
                                    ObjectEntity level3 = new ObjectEntity(m,s.concat(":").concat("method"));
                                    level3.setParent(level2);
                                });
                            }
                        });
                    });
                });

            }
        });

        return deployments;
    }
}