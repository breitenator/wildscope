package org.wildscope.service.objects;

import org.wildscope.service.common.Metric;
import org.wildscope.service.common.MetricValue;
import org.wildscope.service.common.ObjectEntity;
import org.wildscope.service.common.Sample;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Logger;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SamplingService {
    @PersistenceContext(unitName="production")
    private EntityManager entityManager;

    @Inject
    ObjectInfoService objectInfoService;

    @Inject
    Logger log;
    
    public long createSample() {
        Sample sample = new Sample();

        sample.setSampledAt(new Timestamp((new Date()).getTime()));

        entityManager.persist(sample);

        return sample.getSampleId();
    }

    public Sample findLatestSample() {
        List<Sample> result = entityManager.createQuery(
                "select s from Sample s where s.sampledAt = (select max(s2.sampledAt) from Sample s2)", org.wildscope.service.common.Sample.class
        ).getResultList();
        if(result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public Metric findOrCreateMetricByNameAndType(String metricName, String objectType) {
        List<Metric> result = entityManager.createQuery(
                "select m from Metric m where m.metricName = :metricName and m.objectType = :objectType", org.wildscope.service.common.Metric.class
        )
            .setParameter("metricName", metricName)
            .setParameter("objectType", objectType)
            .getResultList();

        if(result.size() == 0){
            log.finer("Adding new metric [" + metricName + "][" + objectType + "]");

            Metric metric = new Metric(metricName,objectType);
            entityManager.persist(metric);
            return metric;
        } else {
            return result.get(0);
        }
    }

    public List<MetricValue> findLatestMetricsValuesByObjectAndSample(long objectId, long sampleId) {
        List<MetricValue> result = entityManager.createQuery(
                "select mv from MetricValue mv where mv.objectId = :objectId and mv.sampleId = :sampleId",
                org.wildscope.service.common.MetricValue.class
        )
                .setParameter("objectId", objectId)
                .setParameter("sampleId", sampleId)
                .getResultList();
        return result;
    }

    public List<ObjectEntity> findObjectsByType(String objectType) {
        List<ObjectEntity> result = entityManager.createQuery(
                "select o from ObjectEntity o where o.objectType = :objectType",
                org.wildscope.service.common.ObjectEntity.class
        )
                .setParameter("objectType", objectType)
                .getResultList();
        return result;
    }

    public ObjectEntity findObjectsById(long objectId) {
        List<ObjectEntity> result = entityManager.createQuery(
                "select o from ObjectEntity o where o.objectId = :objectId",
                org.wildscope.service.common.ObjectEntity.class
        )
                .setParameter("objectId", objectId)
                .getResultList();
        if(result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public Metric findMetricById(long metricId) {
        List<Metric> result = entityManager.createQuery(
                "select m from Metric m where m.metricId = :metricId",
                org.wildscope.service.common.Metric.class
        )
                .setParameter("metricId", metricId)
                .getResultList();
        if(result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public void cleanAll() {
        log.info("Clean all");
        entityManager.createQuery("delete from MetricValue").executeUpdate();
        entityManager.createQuery("delete from Sample").executeUpdate();
        entityManager.createQuery("delete from Metric").executeUpdate();
        entityManager.createQuery("delete from ObjectEntity").executeUpdate();
    }

    private void populateObjectEntityMap(Map<String,ObjectEntity> persistedObjectEntities, ObjectEntity parent) {
        if(!persistedObjectEntities.containsKey(parent.path())) {
            log.finer("Adding object with all children [" + parent.path() + "][" + parent + "]");
            if(parent.getParent() == null) {
                persistedObjectEntities.put(parent.path(), parent);
            } else {
                parent.setParent(persistedObjectEntities.get(parent.getParent().path()));
            }
            return;
        }
        if(parent.getChildren() != null) {
            parent.getChildren().forEach(child -> {
                        populateObjectEntityMap(persistedObjectEntities, child);
                    }
            );
        }
    }

    private void mergeObjects(Set<String> deployments) {
        Map<String,ObjectEntity> persistedObjectEntities = new HashMap<>();
        entityManager.createQuery("select o from ObjectEntity o", ObjectEntity.class).getResultList().
                forEach(entity -> persistedObjectEntities.put(entity.path(), entity));

        List<ObjectEntity> deploymentsSample = objectInfoService.itemListToObjectEntityTree();

        deploymentsSample.forEach(deployment -> {
            if(deployments == null || deployments.contains(deployment.getObjectName())) {
                populateObjectEntityMap(persistedObjectEntities, deployment);
                entityManager.merge(persistedObjectEntities.get(deployment.path()));
            }
        });
    }

    public String getIncrementalValue(String value, String previousValue) {
        try {
            long longValue = Long.parseLong(value);
            if(previousValue != null) {
                longValue -= Long.parseLong(previousValue);

                value = "" + longValue;
            }
        } catch (NumberFormatException ex) {}

        return value;
    }

    public void sampleMetrics(Set<String> deployments) {
        log.info("sample metrics for deployment [" + deployments + "]");

        mergeObjects(deployments);

        List<ObjectEntity> objects = entityManager.createQuery("select o from ObjectEntity o where o.objectType not in ('deployment')", ObjectEntity.class).getResultList();
        Set<MetricValue> metricsValues = new HashSet<>();

        Sample latestSample = findLatestSample();


        long sampleId = createSample();

        objects.forEach(object -> {
            Map<String, Object> metrics;

            long latestSampleId = 0;
            if (latestSample != null) latestSampleId = latestSample.getSampleId();

            List<MetricValue> previousValues = findLatestMetricsValuesByObjectAndSample(object.getObjectId(), latestSampleId);

            if (object.getObjectType().equals("ejb3:method")) {

                metrics = objectInfoService.getMethod(
                        object.getRoot().getObjectName(),
                        object.getParent().getObjectType().split(":")[0],
                        object.getParent().getObjectType().split(":")[1],
                        object.getParent().getObjectName(),
                        object.getObjectName());

                metrics.keySet().stream().forEach(metricKey -> {

                    Metric metric = findOrCreateMetricByNameAndType(metricKey, object.getObjectType());

                    String previousValue = "";
                    for (MetricValue previousValueTemp : previousValues) {
                        if (previousValueTemp.getMetricId().equals(metric.getMetricId())) {
                            previousValue = previousValueTemp.getMetricTotalValue();
                            break;
                        }
                    }

                    metricsValues.add(
                            new MetricValue(
                                    object.getObjectId(),
                                    metric.getMetricId(),
                                    sampleId,
                                    getIncrementalValue(metrics.get(metricKey).toString(), previousValue),
                                    metrics.get(metricKey).toString()));
                });
            } else {

                metrics = objectInfoService.getObject(
                        object.getRoot().getObjectName(),
                        object.getObjectType().split(":")[0],
                        object.getObjectType().split(":")[1],
                        object.getObjectName());

                metrics.keySet().stream().forEach(metricKey -> {

                    String[] name = metricKey.split("/");

                    if (name.length == 3) {
                        Metric metric = findOrCreateMetricByNameAndType(name[name.length - 1], object.getObjectType());

                        String previousValue = "";
                        for (MetricValue previousValueTemp : previousValues) {
                            if (previousValueTemp.getMetricId().equals(metric.getMetricId())) {
                                previousValue = previousValueTemp.getMetricTotalValue();
                                break;
                            }
                        }

                        metricsValues.add(new MetricValue(
                                object.getObjectId(),
                                metric.getMetricId(),
                                sampleId,
                                getIncrementalValue(metrics.get(metricKey).toString(), previousValue),
                                metrics.get(metricKey).toString()));
                    }
                });
            }
        });

        metricsValues.stream().forEach(m -> entityManager.persist(m));
    }
}
