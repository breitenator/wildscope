package org.wildscope.service.objects;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

@Path("info")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class DictionaryResource {

    public class Record {
        private String id;
        private String text;

        public Record(String id, String text) {
            this.id = id;
            this.text = text;
        }
    }

    @Inject
    private SamplingService samplingService;

    @Path("/ejb")
    @GET
    public List<Record> getEJBs(@QueryParam("objectTypes") String[] objectTypes) {
        List<Record> result = new LinkedList<>();
        for(String t : objectTypes) {
            samplingService.findObjectsByType(t)
                    .forEach(o -> result.add(new Record("" + o.getObjectId(), o.getObjectName())));
        }

        return result;
    }

    @Path("/ejbWithCategories")
    @GET
    public Map<String,List<Record>> getEJBsWithCategories(@QueryParam("objectTypes") String[] objectTypes) {
        Map<String,List<Record>> result = new HashMap<>();
        for(String t : objectTypes) {
            samplingService.findObjectsByType(t).forEach(o -> {
                if (!result.containsKey(o.getObjectType())) {
                    result.put(o.getObjectType(), new ArrayList<>());
                }
                result.get(o.getObjectType()).add(new Record("" + o.getObjectId(), o.getObjectName()));
            });
        }

        return result;
    }
}
