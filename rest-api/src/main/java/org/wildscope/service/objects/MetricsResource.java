package org.wildscope.service.objects;

import org.wildscope.logging.Logged;
import org.wildscope.service.common.Metric;
import org.wildscope.service.common.ObjectEntity;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Logged
@Path("metrics")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class MetricsResource {

    @Inject
    ObjectInfoService service;

    @Inject
    SamplingService samplingService;

    @PersistenceContext(unitName="production")
    EntityManager em;

    @Path("/init")
    @POST
    public Response initMetrics() {

        List<ObjectEntity> objects = em.createQuery("select o from ObjectEntity o where o.parentObjectEntity is null", ObjectEntity.class).getResultList();
        Set<Metric> metrics = new HashSet<>();

        objects.forEach(p -> {
            p.getChildren().forEach(o -> {
                if (!o.getObjectType().endsWith("method")) {
                    service.getObject(
                            o.getParent().getObjectName(),
                            o.getObjectType().split(":")[0],
                            o.getObjectType().split(":")[1],
                            o.getObjectName())
                            .keySet().stream().forEach(k ->
                    {
                        String[] name = k.split("/");
                        metrics.add(new Metric(name[name.length - 1], o.getObjectType()));
                    });

                }
            });
        });
        metrics.forEach(m -> em.persist(m));
        return Response.ok().build();
    }

    @Path("/loopSampleMetrics")
    @POST
    public Response loopSampleMetrics() {
        Runnable task = () -> {
            while (true) {
                samplingService.sampleMetrics(null);
                try {
                    Thread.sleep(3000);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        };

        new Thread(task).start();

        return Response.ok().build();
    }

    @Path("/sampleMetrics")
    @POST
    public Response sampleMetrics() {
        samplingService.sampleMetrics(null);

        return Response.ok().build();
    }
}
