package org.wildscope.service.objects;

import org.wildscope.logging.ExcludeLoggedInterceptor;
import org.wildscope.logging.Logged;
import org.wildscope.provider.Pretty;
import org.wildscope.service.settings.SettingsService;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.logging.Logger;

@Logged
@Path("sampling")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Singleton
@Startup
@DependsOn("ApplicationStartup")
@Lock(LockType.READ)
public class SamplingController {
    @Inject
    private SettingsService settingsService;

    public static class Request {
        private Integer frequency = 5;
        private String[] deployments = new String[0];

        @Override
        public String toString() {
            return "Request{" +
                    "frequency=" + frequency +
                    ", deployments=" + Arrays.toString(deployments) +
                    '}';
        }
    }

    public static class Status {
        public static final String STATUS_ACTIVE = "active";
        public static final String STATUS_INACTIVE = "inactive";
        public static final String STATUS_PAUSE = "pause";
        private String status = "";

        public Status(String status) {
            this.status = status;
        }

        public boolean isActive() {
            return this.status.equals(STATUS_ACTIVE);
        }

        public String getStatus() {
            return status;
        }
    }

    @Resource
    private TimerService timerService;

    @Inject
    private Logger log;

    @Inject
    SamplingService samplingService;

    private Timer timer;

    private Map<String, Status> deployments = new Hashtable<>();

    @Path("/status")
    @GET @Pretty
    @ExcludeLoggedInterceptor
    public Map<String, Status> status() {
        return deployments;
    }

    @Path("/start")
    @POST @Pretty
    public Response startSampling(Request request) {
        log.info("startSampling [" + request + "]");

        boolean cleanUp = false;
        if(getDeploymentsByStatus(Status.STATUS_ACTIVE).size() == 0 && getDeploymentsByStatus(Status.STATUS_PAUSE).size() == 0) {
            cleanUp = true;
        }

        Arrays.asList(request.deployments).forEach(deployment -> {
            if(deployments.containsKey(deployment)) {
                deployments.remove(deployment);
            }
            deployments.put(deployment, new Status(Status.STATUS_ACTIVE));
        });

        Response result = Response.noContent().build();
        if (request.frequency == null) {
            result = Response.status(Response.Status.BAD_REQUEST).build();
        } else if (timer == null) {
            if(cleanUp) {
                    samplingService.cleanAll();
            }
            TimerConfig timerConfig = new TimerConfig();
            timerConfig.setPersistent(false);
            ScheduleExpression schedule = new ScheduleExpression();
            schedule.year("*").month("*").dayOfMonth("*").hour("*").minute("*").second("*/" + settingsService.findAssemblingFrequency());
            timer = timerService.createCalendarTimer(schedule, timerConfig);
        }

        return result;
    }

    @Path("/stop")
    @POST @Pretty
    public Response stopSampling(String[] response) {
        String deployment = response[0];
        boolean pause = Boolean.parseBoolean(response[1]);
        log.info("stopSampling [" + deployment + "] pause [" + pause + "]");

        if(deployments.containsKey(deployment)) {
            deployments.remove(deployment);
            deployments.put(deployment,new Status(pause?Status.STATUS_PAUSE:Status.STATUS_INACTIVE));
        }

        Response result = Response.noContent().build();

        boolean shutdown = true;
        for(Map.Entry<String,Status> status: deployments.entrySet()) {
            if(status.getValue().isActive()) shutdown = false;
        }

        if (shutdown && timer != null) {
            timer.cancel();
            timer = null;
        }
        return result;
    }

    @Timeout
    public void onTimeout(Timer timer) {
        samplingService.sampleMetrics(getDeploymentsByStatus(Status.STATUS_ACTIVE));
    }

    private Set<String> getDeploymentsByStatus(String status) {
        Set<String> activeDeployments = new HashSet();
        for(Map.Entry<String,Status> deployment : deployments.entrySet()) {
            if(deployment.getValue().getStatus().equals(status)) {
                activeDeployments.add(deployment.getKey());
            }
        }
        return  activeDeployments;
    }


}
