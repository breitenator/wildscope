package org.wildscope.service.objects;

import org.wildscope.logging.Logged;
import org.wildscope.provider.Pretty;
import org.wildscope.service.common.ObjectEntity;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Logged
@Path("objects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class ObjectsResource {

    @Inject
    ObjectInfoService service;

    @PersistenceContext(unitName="production")
    EntityManager em;

    @GET @Pretty
    public Map<String, Map<String, String>> list() {

        Map<String, Map<String, String>> result = new HashMap<>();
        service.listDeployments().stream().forEach((d) -> {
            List<String> subsystem = service.listSubsystems(d);
            if (!subsystem.isEmpty()) {
                Map<String, String> modules = new HashMap<>();
                subsystem.stream().forEach(s -> {
                    service.listSubsystem(d, s).forEach(c -> {
                        service.listCategory(d, s, c).stream().forEach(it -> {
                            modules.put(it, s.concat(":").concat(c));
                            if (s.equalsIgnoreCase("ejb3")) {
                                service.getObject(d, s, c, it)
                                    .keySet().stream()
                                    .filter(k -> k.startsWith("/result/methods"))
                                    .forEach(m -> {
                                        modules.put(it.concat(":").concat(m.split("/")[3]), s.concat(":").concat("method"));
                                    });
                            }
                        });
                    });
                });
                result.put(d, modules);
            }
        });
        return result;
    }

    @Path("/init")
    @POST @Pretty
    public Response initObjects() {
        service.listDeployments().stream().forEach((d) -> {
            ObjectEntity deployment = new ObjectEntity(d, "deployment");
            List<String> subsystem = service.listSubsystems(d);
            if (!subsystem.isEmpty()) {
                subsystem.stream().forEach(s -> {
                    service.listSubsystem(d, s).forEach(c -> {
                        service.listCategory(d, s, c).stream().forEach(it -> {
                            ObjectEntity object = new ObjectEntity(it, s.concat(":").concat(c));
                            object.setParent(deployment);
                            if (s.equalsIgnoreCase("ejb3")) {
                                service.getObject(d, s, c, it)
                                        .keySet().stream()
                                        .filter(k -> k.startsWith("/result/methods"))
                                        .forEach(m -> {
                                            ObjectEntity method = new ObjectEntity(m.split("/")[3], s.concat(":").concat("method"));
                                            method.setParent(object);
                                        });
                            }
                        });
                    });
                });
                em.persist(deployment);
                em.flush();
            }
        });
        return Response.ok().build();
    }

    @Path("/initTree")
    @POST @Pretty
    public Response initObjectsTree() {
        List<ObjectEntity> deployments = service.itemListToObjectEntityTree();

        deployments.stream().forEach(d -> { em.persist(d); });

        em.flush();

        return Response.ok().build();
    }

    @Path("/initDeployments")
    @POST @Pretty
    public Response initDeployments() {
        List<ObjectEntity> deployments = service.itemListToObjectEntityTree();

        deployments.stream().forEach(deployment -> {
            deployment.getChildren().clear();
            em.persist(deployment);
        });

        em.flush();

        return Response.status(201).entity(deployments).build();
    }
}