package org.wildscope.service.settings;

import org.wildscope.service.common.SettingEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class SettingsService {
    @PersistenceContext(unitName="production")
    private EntityManager entityManager;

    public List<SettingEntity> findAll() {
        return entityManager.createQuery(
                "select s from SettingEntity s",
                SettingEntity.class)
                .getResultList();
    }
    
    public SettingEntity findByName(String propertyName) {
        SettingEntity result = entityManager.createQuery(
                "select s from SettingEntity s where s.name = :propertyName",
                SettingEntity.class
        )
                .setParameter("propertyName", propertyName)
                .getSingleResult();

        return result;
    }

    public void updateProperty(String name, String value) {
        SettingEntity p = findByName(name);
        p.setValue(value);
        entityManager.merge(p);
    }

    public String findHostName() {
        return findByName("hostname").getValue();
    }

    public int findPort() {
        return Integer.parseInt(findByName("port").getValue());
    }

    public String findUsername() {
        return findByName("username").getValue();
    }

    //TODO encrypt password
    public String findPassword() {
        return findByName("password").getValue();
    }

    public int findAssemblingFrequency() {
        return Integer.parseInt(findByName("assembling_frequency").getValue());
    }
}
