package org.wildscope.service.settings;

import org.wildscope.provider.Pretty;
import org.wildscope.service.common.SettingEntity;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@Path("settings")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class SettingsResource {
    @Inject
    private SettingsService settingsService;

    @GET
    public List<SettingEntity> getSettings() {
        return settingsService.findAll();
    }

    @Path("/{settingName}")
    @GET
    public String getSettingByName(
            @PathParam("settingName") String settingName
    ) {
        if(settingName.equals("username") || settingName.equals("password")) {
            return "";
        }
        return settingsService.findByName(settingName).getValue();
    }

    @POST @Pretty
    public Response saveSettings(Map<String, String> properties){
        Response result = Response.accepted().build();
        properties.entrySet().forEach(e -> {
            settingsService.updateProperty(e.getKey(), e.getValue());
        });

        return result;
    }
}
