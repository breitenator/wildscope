package org.wildscope.service.dummy;

import org.wildscope.logging.Logged;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

@Path("dummy")
@Logged
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class DummyResource {
    @Inject
    private Logger log;

    @Path("/dummyMethod1")
    @GET
    public Response dummyMethod1() {
        log.info("dummyMethod1 start");

        //saveMethodCall("dummyMethod1");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("dummyMethod1 end");

        return Response.ok().build();
    }

    @Path("/dummyMethod2")
    @GET
    public Response dummyMethod2() {
        log.info("dummyMethod2 start");

        //saveMethodCall("dummyMethod2");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("dummyMethod2 end");

        return Response.ok().build();
    }

    @Path("/dummyMethod3/{settingName}")
    @GET
    public Response dummyMethod3(
        @PathParam("settingName") String settingName,
        @QueryParam("objectId") long objectId
    ) {
        log.info("dummyMethod3 start");

        //saveMethodCall("dummyMethod2");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("dummyMethod3 end");

        return Response.ok().build();
    }

    private void saveMethodCall(String message) {
        Date dNow = new Date( );
        SimpleDateFormat ft =
                new SimpleDateFormat ("yyyy.MM.dd hh:mm:ss");

        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("F:\\projects\\wildscope\\out\\dummyCalls.txt", true)))) {
            out.println(ft.format(dNow) + ": " + message);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
