package org.wildscope.service.statistics;

import org.wildscope.service.common.MetricValue;
import org.wildscope.service.common.Sample;
import org.wildscope.service.charts.ChartSeries;
import org.wildscope.service.objects.SamplingService;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class StatisticsService {
    @PersistenceContext(unitName="production")
    private EntityManager entityManager;

    @Inject
    private SamplingService samplingService;

    public List<MetricValue> findEJBMetricsValuesByObjectId(long objectId, long sampleFromId, List<String> metricNames) {
        List<MetricValue> result = entityManager.createQuery(
                "select mv " +
                        "from " +
                        "  MetricValue mv, " +
                        "  ObjectEntity oe, " +
                        "  Metric m " +
                        "where " +
                        "  mv.objectId = oe.objectId and " +
                        "  mv.metricId = m.metricId and " +
                        "  oe.objectId = :objectId and " +
                        "  mv.sampleId >= :sampleFromId and " +
                        "  m.metricName in (:metricNames) " +
                        "order by mv.sampleId, mv.objectId",
                org.wildscope.service.common.MetricValue.class
        )
                .setParameter("objectId",objectId)
                .setParameter("sampleFromId", sampleFromId)
                .setParameter("metricNames", metricNames)
                .getResultList();
        return result;
    }

    public List<MetricValue> findEJBMetricsValuesByParentObjectId(long parentObjectId, long sampleFromId, List<String> metricNames) {
        List<MetricValue> result = entityManager.createQuery(
                "select mv " +
                "from " +
                "  MetricValue mv, " +
                "  ObjectEntity oe, " +
                "  Metric m " +
                "where " +
                "  mv.objectId = oe.objectId and " +
                "  mv.metricId = m.metricId and " +
                "  oe.parentObjectEntity.objectId = :objectId and " +
                "  mv.sampleId >= :sampleFromId and " +
                "  m.metricName in (:metricNames) " +
                "order by mv.sampleId, mv.objectId",
                org.wildscope.service.common.MetricValue.class
        )
                .setParameter("objectId",parentObjectId)
                .setParameter("sampleFromId", sampleFromId)
                .setParameter("metricNames", metricNames)
                .getResultList();
        return result;
    }

    public List<MetricValue> findEJBMetricsValuesByParentObjectIdLast(long parentObjectId, List<String> metricNames) {
        List<MetricValue> result = entityManager.createQuery(
                "select mv " +
                        "from " +
                        "  MetricValue mv, " +
                        "  ObjectEntity oe, " +
                        "  Metric m " +
                        "where " +
                        "  mv.objectId = oe.objectId and " +
                        "  mv.metricId = m.metricId and " +
                        "  oe.parentObjectEntity.objectId = :objectId and " +
                        "  mv.sampleId = ( " +
                        "    select max(mv2.sampleId)" +
                        "    from MetricValue mv2" +
                        "    where " +
                        "      mv2.objectId = mv.objectId and " +
                        "      mv2.metricId = mv.metricId " +
                        "  ) and " +
                        "  m.metricName in (:metricNames) " +
                        "order by mv.sampleId, mv.objectId",
                org.wildscope.service.common.MetricValue.class
        )
                .setParameter("objectId",parentObjectId)
                .setParameter("metricNames", metricNames)
                .getResultList();
        return result;
    }

    public List<MetricValue> findEJBMetricsValuesByParentObjectIdFirst(long parentObjectId, List<String> metricNames) {
        List<MetricValue> result = entityManager.createQuery(
                "select mv " +
                        "from " +
                        "  MetricValue mv, " +
                        "  ObjectEntity oe, " +
                        "  Metric m " +
                        "where " +
                        "  mv.objectId = oe.objectId and " +
                        "  mv.metricId = m.metricId and " +
                        "  oe.parentObjectEntity.objectId = :objectId and " +
                        "  mv.sampleId = ( " +
                        "    select min(s.sampleId)" +
                        "    from Sample s" +
                        "  ) and " +
                        "  m.metricName in (:metricNames) " +
                        "order by mv.sampleId, mv.objectId",
                org.wildscope.service.common.MetricValue.class
        )
                .setParameter("objectId",parentObjectId)
                .setParameter("metricNames", metricNames)
                .getResultList();
        return result;
    }

    public List<Sample> findSamples(long sampleFromId) {
        List<Sample> result = entityManager.createQuery(
                "select s from Sample s where s.sampleId >= :sampleFromId order by sampleId",
                org.wildscope.service.common.Sample.class
        )
                .setParameter("sampleFromId", sampleFromId)
                .getResultList();

        return result;
    }

    public Map<Long,Sample> findSamplesMap(long sampleFromId){
        Map<Long,Sample> sampleMap = new HashMap<>();
        findSamples(sampleFromId).forEach(sample -> sampleMap.put(sample.getSampleId(),sample));

        return sampleMap;
    }


    public ChartSeries findNextSeries(long objectId, long parentObjectId, long sampleFromId, List<String> metricNames, boolean metricAsSeriesName) {
        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setSampleIdFrom(sampleFromId);
        chartSeries.setSampleIdTo(sampleFromId);

        List<MetricValue> ejbMetricsValues = null;
        if(parentObjectId > 0) {
            ejbMetricsValues = findEJBMetricsValuesByParentObjectId(parentObjectId, sampleFromId, metricNames);
        } else {
            ejbMetricsValues = findEJBMetricsValuesByObjectId(objectId, sampleFromId, metricNames);
        }
        Map<Long,Sample> samplesMap = findSamplesMap(sampleFromId);

        Map<Long,String> seriesNameIds = new HashMap<>();

        if(!metricAsSeriesName) {
            for (MetricValue ejbMetricsValue : ejbMetricsValues) {
                if (!seriesNameIds.containsKey(ejbMetricsValue.getObjectId())) {
                    String objectName = samplingService.findObjectsById(ejbMetricsValue.getObjectId()).getObjectName();

                    chartSeries.getSeries().put(objectName, new ArrayList<>());
                    seriesNameIds.put(ejbMetricsValue.getObjectId(), objectName);
                }

                List value = new ArrayList();
                value.add(samplesMap.get(ejbMetricsValue.getSampleId()).getSampledAt().getTime());
                value.add(Integer.parseInt(ejbMetricsValue.getMetricValue()));
                chartSeries.getSeries().get(seriesNameIds.get(ejbMetricsValue.getObjectId())).add(value);

                chartSeries.setSampleIdTo(ejbMetricsValue.getSampleId());
            }
        } else {
            for (MetricValue ejbMetricsValue : ejbMetricsValues) {
                if (!seriesNameIds.containsKey(ejbMetricsValue.getMetricId())) {
                    String metricName = samplingService.findMetricById(ejbMetricsValue.getMetricId()).getMetricName();

                    chartSeries.getSeries().put(metricName, new ArrayList<>());
                    seriesNameIds.put(ejbMetricsValue.getMetricId(), metricName);
                }

                List value = new ArrayList();
                value.add(samplesMap.get(ejbMetricsValue.getSampleId()).getSampledAt().getTime());
                value.add(Integer.parseInt(ejbMetricsValue.getMetricValue()));
                chartSeries.getSeries().get(seriesNameIds.get(ejbMetricsValue.getMetricId())).add(value);

                chartSeries.setSampleIdTo(ejbMetricsValue.getSampleId());
            }
        }

        return chartSeries;
    }

    public ChartSeries findObjectStatisticsByMetricNamesLatest(long parentObjectId, long sampleFromId, List<String> metricNames) {
        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setSampleIdFrom(sampleFromId);
        chartSeries.setSampleIdTo(sampleFromId);

        List<MetricValue> ejbMetricsValues = findEJBMetricsValuesByParentObjectIdLast(parentObjectId, metricNames);
        List<MetricValue> ejbMetricsValuesFirst = findEJBMetricsValuesByParentObjectIdFirst(parentObjectId, metricNames);

        Map<Long,String> seriesNameIds = new HashMap<>();
        for (MetricValue ejbMetricsValue : ejbMetricsValues) {
            if(!seriesNameIds.containsKey(ejbMetricsValue.getObjectId())) {
                String methodName = samplingService.findObjectsById(ejbMetricsValue.getObjectId()).getObjectName();

                chartSeries.getSeries().put(methodName, new ArrayList<>());
                seriesNameIds.put(ejbMetricsValue.getObjectId(),methodName);
            }

            int firstValue = 0;
            for (MetricValue ejbMetricsValueFirst : ejbMetricsValuesFirst) {
                if(ejbMetricsValueFirst.getMetricId().equals(ejbMetricsValue.getMetricId()) &&
                        ejbMetricsValueFirst.getObjectId().equals(ejbMetricsValue.getObjectId())) {
                    firstValue = Integer.parseInt(ejbMetricsValueFirst.getMetricTotalValue());
                    break;
                }
            }

            int lastValue = Integer.parseInt(ejbMetricsValue.getMetricTotalValue());

            List value = new ArrayList();
            value.add(lastValue - firstValue);
            chartSeries.getSeries().get(seriesNameIds.get(ejbMetricsValue.getObjectId())).add(value);

            chartSeries.setSampleIdTo(ejbMetricsValue.getSampleId());
        }

        return chartSeries;
    }

/*
    public StatisticsHighstock getEJBStatisticsHighchart(long parentObjectId, long sampleFromId, String metricName) {
        StatisticsHighstock statisticsHighstock = new StatisticsHighstock();
        statisticsHighstock.setSampleIdFrom(sampleFromId);
        statisticsHighstock.setSampleIdTo(sampleFromId);

        List<Sample> samples = findSamples(sampleFromId);
        List<MetricValue> ejbMetricsValues = findEJBMetricsValuesByParentObjectId(parentObjectId, sampleFromId, metricName);

        List<String> categories = new LinkedList<>();

        int metricValueIndex = 0;

        Set<Long> objects = new HashSet<>();
        for(int i = 0; i < samples.size(); i++) {
            Sample sample = samples.get(i);

            String formatted = "" + sample.getSampledAt().getTime(); //new SimpleDateFormat("HH:mm:ss").format(sample.getSampledAt()); //"MM/dd/yyyy HH:mm:ss"
            categories.add(formatted);

            int seriesId = 0;
            while(true) {
                if(metricValueIndex >= ejbMetricsValues.size() ||
                        !ejbMetricsValues.get(metricValueIndex).getSampleId().equals(sample.getSampleId())) break;


                MetricValue metricValue = ejbMetricsValues.get(metricValueIndex);

                if(!objects.contains(metricValue.getObjectId())) {
                    statisticsHighstock.getSeries().add(new ArrayList<>());
                    statisticsHighstock.getSeriesNames().add(samplingService.findObjectsById(metricValue.getObjectId()).getObjectName());
                    objects.add(metricValue.getObjectId());

                    for(int j = 0; j < i; j++) {
                        statisticsHighstock.getSeries().get(seriesId).add(null);
                    }
                }

                List value = new ArrayList();
                value.add(sample.getSampledAt().getTime());
                value.add(Integer.parseInt(metricValue.getMetricValue()));
                statisticsHighstock.getSeries().get(seriesId).add(value);

                seriesId++;
                metricValueIndex++;
            }

            statisticsHighstock.setSampleIdTo(sample.getSampleId());
        }

        statisticsHighstock.setCategories(categories);

        return statisticsHighstock;
    }
*/
}
