package org.wildscope.filter;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Provider
@PreMatching
public class HeaderLoggingFilter implements ContainerRequestFilter {

    @Inject
    Logger log;

    @Override
    public void filter(ContainerRequestContext ctx) throws IOException {
        for (Map.Entry<String, List<String>> h : ctx.getHeaders().entrySet()) {
            log.fine(h.getKey() + "=" + h.getValue());
        }
    }
}
