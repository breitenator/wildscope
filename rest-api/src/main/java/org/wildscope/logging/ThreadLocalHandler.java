package org.wildscope.logging;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class ThreadLocalHandler extends Handler {

    private static ThreadLocalHandler instance;

    private List<Handler> handlers;
    private ThreadLocal<List<LogRecord>> recordStore;

    public static ThreadLocalHandler getInstance() {
        synchronized (ThreadLocalHandler.class) {
            if (instance == null) {
                instance = new ThreadLocalHandler(Logger.getAnonymousLogger());
            }
        }
        return instance;
    }

    private ThreadLocalHandler(Logger logInstance) {
        handlers = new ArrayList<>();
        for (Logger l = logInstance; l != null; l = l.getParent()) {
            if (l.getHandlers() != null) {
                Collections.addAll(handlers, l.getHandlers());
            }
        }

        recordStore = new ThreadLocal<List<LogRecord>>() {
            @Override protected List<LogRecord> initialValue () {
                return new ArrayList<>();
            }
        };
    }

    @Override
    public void publish(final LogRecord record) {
        recordStore.get().add(record);
    }

    @Override
    public void flush() {
        List<LogRecord> records = recordStore.get();
        handlers.stream().forEach(h -> {
            records.stream().forEach(r -> h.publish(r));
        });
        records.clear();
    }

    public int size() {
        return recordStore.get().size();
    }

    @Override
    public void close() throws SecurityException {
        recordStore.get().clear();
    }
}
