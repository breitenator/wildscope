package org.wildscope.logging;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;

@Logged
@Interceptor
public class LoggedInterceptor implements Serializable {

    @Inject
    HttpServletRequest req;

    @AroundInvoke
    public Object logMethodEntry(InvocationContext invocationContext) throws Exception {

        if(invocationContext.getMethod().getAnnotation(ExcludeLoggedInterceptor.class) != null) {
            return invocationContext.proceed();
        } else {
            ThreadLocalHandler.getInstance().publish(new LogRecord(Level.INFO, "+[LOG RECORD]------------------"));

            if (req != null) {
                ThreadLocalHandler.getInstance().publish(new LogRecord(Level.INFO, req.getMethod() + " " + req.getRequestURI()));
                StringBuilder sb = new StringBuilder();

                for (Map.Entry<String, String[]> e : req.getParameterMap().entrySet()) {
                    sb.append(e.getKey()).append(": [");
                    for (String s : e.getValue()) {
                        sb.append(s).append(",");
                    }
                    sb.deleteCharAt(sb.length() - 1);
                    sb.append("],");
                }

                if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }

                ThreadLocalHandler.getInstance().publish(new LogRecord(Level.INFO, String.format("{%s}", sb.toString())));
            }

            long startTime = System.currentTimeMillis();
            Object result = invocationContext.proceed();

            if (ThreadLocalHandler.getInstance().size() > 1) {
                StringBuilder time = new StringBuilder();
                time.append("Execution time [").append(System.currentTimeMillis() - startTime).append("] milliseconds");
                ThreadLocalHandler.getInstance().publish(new LogRecord(Level.INFO, time.toString()));
                ThreadLocalHandler.getInstance().publish(new LogRecord(Level.INFO, "-[LOG RECORD]------------------"));
                ThreadLocalHandler.getInstance().flush();
            } else {
                ThreadLocalHandler.getInstance().close();
            }
            return result;
        }
    }
}
