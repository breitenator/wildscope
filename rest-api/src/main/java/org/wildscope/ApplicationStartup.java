package org.wildscope;

import org.wildscope.logging.ThreadLocalHandler;
import org.wildscope.service.common.ResourceClient;
import org.wildscope.service.settings.SettingsService;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.sql.DataSource;
import java.util.logging.Handler;
import java.util.logging.Logger;

@Startup
@Singleton
public class ApplicationStartup {

    @Inject
    private Logger logger;

    @Inject
    private SettingsService settingsService;

    private InitialContext jndiContext;

    private ScriptEngineManager scriptingManager;

    @PostConstruct
    public void onStartup() throws NamingException {
        jndiContext = new InitialContext();
        logger.info("application started");
        scriptingManager = new ScriptEngineManager();

    }

    @Produces
    public ScriptEngine produceScriptEngine() {
        return scriptingManager.getEngineByName("javascript");
    }

    @Produces
    public ResourceClient produceResourceClient() {
        ResourceClient result = new ResourceClient(settingsService.findHostName(), settingsService.findPort());
        result.setCredentials(settingsService.findUsername(), settingsService.findPassword());
        return result;
    }

    @Produces
    @Lock(LockType.WRITE)
    public DataSource getApplicationDataSource() throws NamingException {
        return DataSource.class.cast(jndiContext.lookup("java:/jdbc/datasources/WildscopeDS"));
    }

    public static class LoggerProducer {
        private static final String MainCategory = "wildscope";
        @Produces
        public Logger getApplicationLogger(InjectionPoint injectionPoint) {
            StringBuilder loggerName = new StringBuilder(MainCategory);
            loggerName.append(String.format("::%s", injectionPoint.getMember().getDeclaringClass().getName()));
            Logger result = Logger.getLogger(loggerName.toString());
            result.setUseParentHandlers(false);

            boolean isCustomized = false;
            for(Handler handler : result.getHandlers()) {
                if (handler == ThreadLocalHandler.getInstance()) {
                    isCustomized = true;
                    break;
                }
            }

            if (!isCustomized) {
                result.addHandler(ThreadLocalHandler.getInstance());
            }

            return result;
        }
    }
}
