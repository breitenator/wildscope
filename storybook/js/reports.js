$(function () {
    $("#item-picker").select2();
    $('#container').highcharts({
        title: {
            text: 'Metric Name: invocations',
            x: -20 //center
        },
        subtitle: {
            text: 'ApplicationStartup (ejb3:singleton-bean)',
            x: -20
        },
        xAxis: {
            categories: ['13:10:10', '13:10:15', '13:10:20', '13:10:25', '13:10:30', '13:10:35', '13:10:40', '13:10:45']
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'getApplicationDatasource',
            data: [7.0, 21.0, 26.0, 12.0, 23.0, 25.0, 15.0, 40.0]
        }, {
            name: 'produceScriptingEngine',
            data: [2.0, 2.0, 5.0, 6.0, 2.0, 7.0, 5.0, 8.0]
        }, {
            name: 'produceResourceClient',
            data: [0.0, 1.0, 15.0, 25.0, 12.0, 10.0, 7.0]
        }
        ]
    });
});
