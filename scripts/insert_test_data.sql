insert into samples(sample_id, sampled_at) values (next value for samples_seq, current_timestamp);

insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'wildfly', 0, 'server'
);

insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'heap-memory-usage', (select object_id from objects where object_name = 'wildfly'), 'attribute'
);

insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'non-heap-memory-usage', (select object_id from objects where object_name = 'wildfly'), 'attribute'
);



insert into metrics(
  metric_id, metric_name
) values (
  next value for metrics_seq, 'init'
);

insert into metrics(
  metric_id, metric_name
) values (
  next value for metrics_seq, 'used'
);

insert into metrics(
  metric_id, metric_name
) values (
  next value for metrics_seq, 'committed'
);

insert into metrics(
  metric_id, metric_name
) values (
  next value for metrics_seq, 'max'
);



insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'heap-memory-usage'),
  (select metric_id from metrics m where m.metric_name = 'init'),
  (select max(sample_id) from samples),
  '67108864'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'heap-memory-usage'),
  (select metric_id from metrics m where m.metric_name = 'used'),
  (select max(sample_id) from samples),
  '78124088'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'heap-memory-usage'),
  (select metric_id from metrics m where m.metric_name = 'committed'),
  (select max(sample_id) from samples),
  '197656576'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'heap-memory-usage'),
  (select metric_id from metrics m where m.metric_name = 'max'),
  (select max(sample_id) from samples),
  '477626368'
);





insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'non-heap-memory-usage'),
  (select metric_id from metrics m where m.metric_name = 'init'),
  (select max(sample_id) from samples),
  '2555904'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'non-heap-memory-usage'),
  (select metric_id from metrics m where m.metric_name = 'used'),
  (select max(sample_id) from samples),
  '62360432'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'non-heap-memory-usage'),
  (select metric_id from metrics m where m.metric_name = 'committed'),
  (select max(sample_id) from samples),
  '69509120'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'non-heap-memory-usage'),
  (select metric_id from metrics m where m.metric_name = 'max'),
  (select max(sample_id) from samples),
  '-1'
);




insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'datahub-app.war', 0, 'application'
);

insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'ResultPager', (select object_id from objects where object_name = 'datahub-app.war'), 'stateful-session-bean'
);

insert into metrics(
  metric_id, metric_name
) values (
  next value for metrics_seq, 'execution-time'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'ResultPager'),
  (select metric_id from metrics m where m.metric_name = 'execution-time'),
  (select max(sample_id) from samples),
  '803'
);


insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'init', (select object_id from objects where object_name = 'ResultPager'), 'method'
);

insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'getCurrentForm', (select object_id from objects where object_name = 'ResultPager'), 'method'
);


insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'init'),
  (select metric_id from metrics m where m.metric_name = 'execution-time'),
  (select max(sample_id) from samples),
  '569'
);


insert into metrics(
  metric_id, metric_name
) values (
  next value for metrics_seq, 'invocations'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'init'),
  (select metric_id from metrics m where m.metric_name = 'invocations'),
  (select max(sample_id) from samples),
  '6'
);



insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'getCurrentForm'),
  (select metric_id from metrics m where m.metric_name = 'execution-time'),
  (select max(sample_id) from samples),
  '125'
);



insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'getCurrentForm'),
  (select metric_id from metrics m where m.metric_name = 'invocations'),
  (select max(sample_id) from samples),
  '6'
);






insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'com.company.service.domain.entity.Domain', (select object_id from objects where object_name = 'datahub-app.war'), 'jpa'
);

insert into metrics(
  metric_id, metric_name
) values (
  next value for metrics_seq, 'entity-deleted-count'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'com.company.service.domain.entity.Domain'),
  (select metric_id from metrics m where m.metric_name = 'entity-deleted-count'),
  (select max(sample_id) from samples),
  '0'
);





insert into objects(
  object_id,object_name,object_parent_id, object_type
) values (
  next value for objects_seq, 'jpa', (select object_id from objects where object_name = 'datahub-app.war'), 'attribute'
);

insert into metrics(
  metric_id, metric_name
) values (
  next value for metrics_seq, 'connect-count'
);

insert into metrics_values (
  metric_value_id, object_id, metric_id, sample_id, metric_value
) values (
  next value for metrics_values_seq,
  (select object_id from objects where object_name = 'jpa'),
  (select metric_id from metrics m where m.metric_name = 'connect-count'),
  (select max(sample_id) from samples),
  '0'
);