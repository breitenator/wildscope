drop table metrics_values;
drop table metrics;
drop table objects;
drop table samples;
drop table settings;


drop sequence objects_seq restrict;
drop sequence metrics_seq restrict;
drop sequence samples_seq restrict;
drop sequence metrics_values_seq restrict;
drop sequence settings_seq restrict;
