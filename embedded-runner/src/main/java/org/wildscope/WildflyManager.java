package org.wildscope;

import org.jboss.as.controller.client.helpers.standalone.DeploymentPlan;
import org.jboss.as.controller.client.helpers.standalone.ServerDeploymentManager;
import org.jboss.as.controller.client.helpers.standalone.ServerDeploymentPlanResult;
import org.jboss.as.controller.client.helpers.standalone.ServerDeploymentActionResult;
import org.jboss.as.embedded.EmbeddedServerFactory;
import org.jboss.as.embedded.StandaloneServer;
import org.jboss.dmr.ModelNode;
import org.wildscope.exceptions.ParameterMissingException;
import org.wildscope.exceptions.WildflyException;

import java.io.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.jboss.as.controller.descriptions.ModelDescriptionConstants.OP;
import static org.jboss.as.controller.descriptions.ModelDescriptionConstants.OP_ADDR;

public class WildflyManager {
    private StandaloneServer server;
    private static final String ServerRoot = "JBOSS_HOME";

    private static final String JDBC_URL = "jdbc:derby:%s;create=true";
    
    public WildflyManager() {}

    public void setup() throws ParameterMissingException {
        String serverRoot;
        
        if (System.getenv().containsKey(ServerRoot) && !System.getenv(ServerRoot).isEmpty()) {
            serverRoot = System.getenv(ServerRoot);
        } else if (System.getProperties().containsKey(ServerRoot)) {
            serverRoot = System.getProperty(ServerRoot);
        } else {
            throw new ParameterMissingException("Server root is not specified");
        }

        server = EmbeddedServerFactory.create(serverRoot, null, null, "org.jboss.logmanager");
    }

    public void start() throws WildflyException {
        try {
            server.start();
        } catch (Throwable e) {
            throw new WildflyException("Failed to start server: " + server, e);
        }
    }

    public void deploy(String name, InputStream is) throws InterruptedException, ExecutionException, TimeoutException, IOException {
        ServerDeploymentManager manager = ServerDeploymentManager.Factory.create(server.getModelControllerClient());
        DeploymentPlan plan = manager.newDeploymentPlan()
                .add(name, is).andDeploy()
                .build();
        Future<ServerDeploymentPlanResult> future = manager.execute(plan);
        ServerDeploymentPlanResult result = future.get(20, TimeUnit.SECONDS);
        ServerDeploymentActionResult actionResult = result.getDeploymentActionResult(plan.getId());
        if (actionResult != null) {
            final Throwable deploymentException = actionResult.getDeploymentException();
            if (deploymentException != null) {
                throw new RuntimeException(deploymentException);
            }
        }
    }
/*
    public void deployDriver() throws ClassNotFoundException, IOException, InterruptedException, ExecutionException, TimeoutException {
        File jdbcJar = new File("F:\\projects\\standalone\\derbyclient-10.11.1.1.jar");

        JavaArchive ja = ShrinkWrap.createFromZipFile(JavaArchive.class, jdbcJar);
        final EnterpriseArchive ear = ShrinkWrap.create(EnterpriseArchive.class, "F:\\projects\\standalone\\driver.ear")
                .addAsLibrary(ja)
                .addAsManifestResource(new File("F:\\projects\\wildscope\\embedded-runner\\src\\main\\resources\\derby-ds.xml"))
                .addAsManifestResource(
                        new StringAsset(
                                "Dependencies: org.jboss.ironjacamar.jdbcadapters\n"),
                        "MANIFEST.MF");

        new ZipExporterImpl(ear).exportTo(new File(ear.getName()), true);

        deploy("java:/jdbc/datasources/WildscopeDS",new FileInputStream(new File("F:\\projects\\standalone\\driver.ear")));
    }
*/
    public void addDatasourceConfiguration(String databaseName) throws IOException {

        final ModelNode address = new ModelNode();
        address.add("subsystem", "datasources");
        address.add("data-source", "java:/jdbc/datasources/WildscopeDS");
        address.protect();

        final ModelNode operation = new ModelNode();
        operation.get(OP).set("add");
        operation.get(OP_ADDR).set(address);

        operation.get("jndi-name").set("java:/jdbc/datasources/WildscopeDS");
        operation.get("enabled").set(true);
        operation.get("jta").set(true);
        operation.get("use-ccm").set(true);

        operation.get("connection-url").set(String.format(JDBC_URL, databaseName)); //"jdbc:derby:wildscopetemp;create=true"
        operation.get("driver-class").set("org.apache.derby.jdbc.EmbeddedDriver");
        operation.get("driver-name").set("derby-10.11.1.1.jar");
        operation.get("user-name").set("app");
        operation.get("password").set("app");

        server.getModelControllerClient().execute(operation);
    }

    public void undeploy(String deploymentName) throws ExecutionException, InterruptedException {
        ServerDeploymentManager manager = ServerDeploymentManager.Factory.create(server.getModelControllerClient());
        DeploymentPlan undeployPlan = manager.newDeploymentPlan()
                .undeploy(deploymentName).andRemoveUndeployed()
                .build();
        manager.execute(undeployPlan).get();
    }

    public void stop() throws WildflyException {
        try {
            server.stop();
        } catch (Throwable e) {
            throw new WildflyException("Failed to stop server: " + server, e);
        }
    }
}
