package org.wildscope;

import org.wildscope.exceptions.WildflyException;

import java.util.Date;

public class Run {
    
    public static void main(String[] args) {
        WildflyManager wildflyManager = new WildflyManager();

        try {
            log_info("Setting up server environment");
            wildflyManager.setup();

            log_info("Starting application server");
            wildflyManager.start();

            log_info("Deploy wildscope driver");
            wildflyManager.deploy("derby-10.11.1.1.jar", Run.class.getResourceAsStream("/dist/derby-10.11.1.1.jar"));

            log_info("Apply configuration");
            wildflyManager.addDatasourceConfiguration("wildscopetemp");

            log_info("Deploy wildscope application");
            wildflyManager.deploy("wildscope.war", Run.class.getResourceAsStream("/dist/wildscope.war"));

            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    try {
                        log_info("Undeploy wildscope application");
                        wildflyManager.undeploy("wildscope.war");
                        log_info("Undeploy wildscope datasource configuration");
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        log_info("Stopping application server");
                        try {
                            wildflyManager.stop();
                        } catch (WildflyException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void log_info(String message) {
        Date ts = new Date();
        System.out.println(String.format("%TH:%TM:%TS,%TL INFO  [%s] %s", ts, ts, ts, ts, Run.class.getName(), message));
    }
}
