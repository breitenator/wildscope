package org.wildscope.exceptions;

public class WildflyException extends Exception{
    public WildflyException(String msg, Throwable e) {
        super(msg,e);
    }
}
