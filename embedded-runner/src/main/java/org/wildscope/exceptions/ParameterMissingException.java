package org.wildscope.exceptions;

public class ParameterMissingException extends Exception {
    public ParameterMissingException (String msg) {
        super(msg);
    }
}
